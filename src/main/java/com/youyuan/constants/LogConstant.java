package com.youyuan.constants;

/**
 * 类名称：LogConstant <br>
 * 类描述： 日志相关常量 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 8:57<br>
 */
public class LogConstant {

    /**
     * 日志前缀
     */
    public static final String LOG_PREFIX = "【日志】-";

    /**
     * 关联业务Id
     */
    public static final String LOG_RELATE_ID = "log_relate_id";

    /**
     * 旧数据
     */
    public static final String LOG_OLD_DATA = "log_old_data";

    /**
     * 新数据
     */
    public static final String LOG_NEW_DATA = "log_new_data";

    /**
     * 冒号分隔符
     */
    public static final String COLON_SEPARATOR = "：";

    /**
     * 常量
     */
    public static final Integer ZERO = 0;


}
