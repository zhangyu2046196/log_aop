package com.youyuan.annotation;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.youyuan.constants.LogConstant;
import com.youyuan.entity.OperateLog;
import com.youyuan.service.OperateLogService;
import com.youyuan.util.ContextHandler;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * 类名称：LogAspect <br>
 * 类描述： 日志切面 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:35<br>
 */
@Aspect
@Component
@Slf4j
public class LogAspect {

    private Executor executor = Executors.newCachedThreadPool();

    @Resource
    private OperateLogService operateLogService;

    @Pointcut("@annotation(com.youyuan.annotation.Log)")
    public void logPointCut() {
    }

    @AfterReturning("logPointCut()")
    public void doAfterReturning(JoinPoint point) {
        try {

            List<OperateLog> operateLogs = CollUtil.newArrayList();

            OperateLog operateLog = new OperateLog();

            //获取方法参数
            MethodSignature signature = (MethodSignature) point.getSignature();
            //获取注解对象
            Log businessLog = signature.getMethod().getAnnotation(Log.class);
            String logLevel = businessLog.logLevel().getValue();
            String logType = businessLog.logType().getValue();
            String operationType = businessLog.operationType().toString();
            String operateContext = businessLog.operateContext();
            String operatePath = businessLog.operatePath().getValue();

            operateLog.setLogLevel(logLevel);
            operateLog.setLogType(logType);
            operateLog.setOperationType(operationType);
            operateLog.setOperatePath(operatePath);
            operateLog.setOperateCode("工号");
            operateLog.setOperateName("姓名");

            if (businessLog.isSaveUpdateInfo()) {
                switch (businessLog.operationType()) {
                    case UPDATE: //修改
                        Object old = ContextHandler.get(LogConstant.LOG_OLD_DATA);
                        Map<String, Object> oldMap = new HashMap<>(16);
                        Class oldClass = old.getClass();
                        doWhile(oldMap, oldClass, old);

                        Object newObject = ContextHandler.get(LogConstant.LOG_NEW_DATA);
                        Map<String, Object> newMap = new HashMap<>(16);
                        Class newClass = newObject.getClass();
                        doWhile(newMap, newClass, newObject);
                        operateLog.setContexts(compareUpdateInfo(oldMap, newMap));
                        if (CollUtil.isNotEmpty(operateLog.getContexts())) {
                            operateLogs.add(operateLog);
                        }
                        break;
                    case ADD: //新增
                    case DELETE: //删除
                        Object addOrDelObject = ContextHandler.get(LogConstant.LOG_NEW_DATA);
                        if (addOrDelObject instanceof List) {
                            List<Object> objs = (List<Object>) addOrDelObject;
                            for (Object obj : objs) {
                                List<String> stringList = CollUtil.newArrayList();
                                Map<String, Object> addOrDelMap = new HashMap<>(16);
                                Class addOrDelClass = obj.getClass();
                                doWhile(addOrDelMap, addOrDelClass, obj);
                                OperateLog operateLog1 = BeanUtil.copyProperties(operateLog, OperateLog.class);
                                compareAddOrDelInfo(addOrDelMap, stringList);
                                operateLog1.setContexts(stringList);
                                if (CollUtil.isNotEmpty(operateLog1.getContexts())) {
                                    operateLogs.add(operateLog1);
                                }
                            }
                        } else {
                            List<String> stringList = CollUtil.newArrayList();
                            Map<String, Object> addOrDelMap = new HashMap<>(16);
                            Class addOrDelClass = addOrDelObject.getClass();
                            doWhile(addOrDelMap, addOrDelClass, addOrDelObject);
                            if (CollUtil.isEmpty(addOrDelMap)) {
                                buildAddOrDelInfo(addOrDelObject, stringList);
                            } else {
                                compareAddOrDelInfo(addOrDelMap, stringList);
                            }
                            operateLog.setContexts(stringList);
                            if (CollUtil.isNotEmpty(operateLog.getContexts())) {
                                operateLogs.add(operateLog);
                            }
                        }
                        break;
                    default:
                        break;
                }
                log.info(LogConstant.LOG_PREFIX + "保存日志信息={}", JSON.toJSONString(operateLogs));
                if (CollUtil.isNotEmpty(operateLogs)) {
                    executor.execute(() -> operateLogService.saveLog(operateLogs));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改操作日志记录失败");
        }
    }

    private String getContext(IsSaveUpdateLog isSaveUpdateLog, Object oldObject, Object newObject, Field oldField,
                              Field newField) throws Exception {
        String context = null;
        Object oldValue = getValue(isSaveUpdateLog, oldObject, oldField);
        Object newValue = getValue(isSaveUpdateLog, newObject, newField);
        if (oldValue == null && newValue != null) {
            context = "【" + isSaveUpdateLog.name() + "】由【" + "】修改为【" + newValue + "】";
        }
        if (oldValue != null && newValue == null) {
            context = "【" + isSaveUpdateLog.name() + "】由【" + oldValue + "】修改为【" + "】";
        }
        if (oldValue != null && newValue != null) {
            if (!oldValue.equals(newValue)) {
                context = "【" + isSaveUpdateLog.name() + "】由【" + oldValue + "】修改为【" + newValue + "】";
            }
        }
        if (context != null) {
            log.info(context);
        }
        return context;
    }

    private Object getValue(IsSaveUpdateLog isSaveUpdateLog, Object object, Field field) throws Exception {
        Object value = field.get(object);
        //特殊字段处理
        if (value != null) {
            if (field.getType().getSimpleName().equals("BigDecimal")) {
                value = (new BigDecimal(value.toString())).setScale(2, BigDecimal.ROUND_HALF_UP);
            }

            if (field.getType().getSimpleName().equals("LocalDateTime")) {
                value = ((LocalDateTime) value).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:SS"));
            }
        }
        //是否有枚举依赖
        boolean isRelyEnum = isSaveUpdateLog.isRelyEnum();
        //依赖的后缀字段
        String suffixFieldName = isSaveUpdateLog.suffixFieldName();
        //后缀是否有枚举依赖
        boolean isSuffixFieldEnum = isSaveUpdateLog.isSuffixFieldEnum();
        if (isRelyEnum) {
            value = isSaveUpdateLog.relyEnumClass().getMethod("getValue", String.class).invoke(isSaveUpdateLog.relyEnumClass(), value);
        }
        if (StrUtil.isNotBlank(suffixFieldName)) {
            Field suffixField = null;
            Class tempClass = object.getClass();
            while (tempClass != null) {
                try {
                    suffixField = tempClass.getDeclaredField(suffixFieldName);
                    suffixField.setAccessible(true);
                } catch (Exception e) {

                }
                tempClass = tempClass.getSuperclass();
            }
            Object suffixFieldValue = suffixField.get(object);
            if (suffixFieldValue != null && isSuffixFieldEnum) {
                suffixFieldValue =
                        isSaveUpdateLog.suffixFieldRelyEnumClass().getMethod("getValue", String.class).invoke(isSaveUpdateLog.suffixFieldRelyEnumClass(), suffixFieldValue.toString());
            }
            if (suffixFieldValue != null && value != null) {
                //特殊值处理
                if (field.getName().equals("qualityDate") && "-1".equals(value.toString())) {
                    value = "长期有效";
                } else {
                    value = value.toString() + suffixFieldValue.toString();
                }
            }
        }
        return value;
    }


    private void compareUpdateInfo1(Object oldObject, Object newObject, List<String> stringList) throws Exception {
        List<Field> oldFields = getAll(oldObject.getClass());
        List<Field> newFields = getAll(newObject.getClass());
        for (Field oldField : oldFields) {
            oldField.setAccessible(true);
            IsSaveUpdateLog isSaveUpdateLog = oldField.getAnnotation(IsSaveUpdateLog.class);
            if (isSaveUpdateLog != null) {
                for (Field newField : newFields) {
                    newField.setAccessible(true);
                    if (oldField.getName().equals(newField.getName())) {
                        String context = getContext(isSaveUpdateLog, oldObject, newObject, oldField, newField);
                        if (context != null) {
                            stringList.add(context);
                        }
                    }
                }
            }
        }
    }

    private void buildAddOrDelInfo(Object object, List<String> stringList) throws Exception {
        List<Field> fields = getAll(object.getClass());
        for (Field field : fields) {
            field.setAccessible(true);
            IsSaveUpdateLog isSaveUpdateLog = field.getAnnotation(IsSaveUpdateLog.class);
            if (ObjectUtil.isNotEmpty(isSaveUpdateLog)) {
                Object fieldValue = getValue(isSaveUpdateLog, object, field);
                if (ObjectUtil.isNotEmpty(fieldValue)) {
                    stringList.add(StrUtil.join(LogConstant.COLON_SEPARATOR, isSaveUpdateLog.name(), fieldValue));
                }
            }
        }
    }

    private List<Field> getAll(Class tempClass) {
        List<Field> allFieldList = new ArrayList<>();
        while (tempClass != null) {
            Arrays.stream(tempClass.getDeclaredFields()).forEach(field -> allFieldList.add(field));
            tempClass = tempClass.getSuperclass();
        }
        return allFieldList;
    }

    private List<String> compareUpdateInfo(Map<String, Object> oldMap, Map<String, Object> newMap) throws Exception {
        List<String> stringList = CollUtil.newArrayList();
        for (String s : newMap.keySet()) {
            if (oldMap.containsKey(s)) {
                Object oldObject = oldMap.get(s);
                Object newObject = newMap.get(s);
                compareUpdateInfo1(oldObject, newObject, stringList);
            }
        }
        return stringList;
    }

    private void compareAddOrDelInfo(Map<String, Object> map, List<String> stringList) throws Exception {
        for (String s : map.keySet()) {
            buildAddOrDelInfo(map.get(s), stringList);
        }
    }

    private void doWhile(Map<String, Object> objectMap, Class tempClass, Object ob) throws Exception {
        while (tempClass != null) {
            List<Field> allFieldList = Arrays.asList(tempClass.getDeclaredFields());
            for (int i = 0; i < allFieldList.size(); i++) {
                Field field = allFieldList.get(i);
                field.setAccessible(true);
                if (field.getName().equals("id") && field.get(ob) != null) {
                    objectMap.put(field.get(ob).toString(), ob);
                }
                if (field.getGenericType().getTypeName().startsWith("com.xr")) {
                    Object ob1 = field.get(ob);
                    if (ob1 != null) {
                        Class tempClass1 = ob1.getClass();
                        doWhile(objectMap, tempClass1, ob1);
                    }
                }
                if (field.getGenericType().getTypeName().startsWith("java.util.List")) {
                    List<Object> objectList = (List<Object>) field.get(ob);
                    if (!CollectionUtils.isEmpty(objectList)) {
                        for (int j = 0; j < objectList.size(); j++) {
                            Object ob2 = objectList.get(j);
                            Class tempClass2 = ob2.getClass();
                            doWhile(objectMap, tempClass2, ob2);
                        }
                    }
                }
            }
            tempClass = tempClass.getSuperclass();
        }
    }

}