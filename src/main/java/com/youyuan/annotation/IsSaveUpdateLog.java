package com.youyuan.annotation;

import java.lang.annotation.*;

/**
 * 类名称：IsSaveUpdateLog <br>
 * 类描述： 日志修改注解 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:23<br>
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IsSaveUpdateLog {

    //字段名称
    String name() default "";

    //是否有枚举依赖
    boolean isRelyEnum() default false;

    //依赖的枚举类
    Class<?> relyEnumClass() default String.class;

    //后缀的组合字段
    String suffixFieldName() default "";

    //后缀是否有枚举依赖
    boolean isSuffixFieldEnum() default false;

    //后缀依赖的枚举类
    Class<?> suffixFieldRelyEnumClass() default String.class;
}