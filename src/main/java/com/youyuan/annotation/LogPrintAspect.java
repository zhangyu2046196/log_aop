package com.youyuan.annotation;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 类名称：LogPrintAspect <br>
 * 类描述： 日志打印切面 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2022/12/05 11:30<br>
 */
@Aspect
@Component
@Slf4j
@Order(1)
public class LogPrintAspect {

    @Pointcut("@annotation(com.youyuan.annotation.LogPrint)")
    public void checkLogPrint() {

    }

    @Around("checkLogPrint()")
    public Object doAppAround(ProceedingJoinPoint pjp) throws Throwable {
        return proceedLogPrint(pjp);
    }

    private Object proceedLogPrint(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
        //计算接口耗时
        TimeInterval timer = DateUtil.timer();
        Object proceed = pjp.proceed();
        LogPrint logPrint = method.getAnnotation(LogPrint.class);
        log.info(logPrint.logPrefix() + logPrint.methodName() + ",请求参数={},返回结果={},耗时={}",
                JSON.toJSONString(pjp.getArgs()), logPrint.showReturnValue() ? JSON.toJSONString(proceed) :
                        StrUtil.EMPTY, timer.interval());
        return proceed;
    }

}
