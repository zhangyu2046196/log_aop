package com.youyuan.annotation;


import com.youyuan.enums.LogLevelEnum;
import com.youyuan.enums.LogTypeEnum;
import com.youyuan.enums.OperationPathEnum;
import com.youyuan.enums.OperationTypeEnum;

import java.lang.annotation.*;

/**
 * 类名称：Log <br>
 * 类描述： 打印修改日志注解 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:24<br>
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    //日志级别
    LogLevelEnum logLevel() default LogLevelEnum.INFO;


    //日志类型
    LogTypeEnum logType() default LogTypeEnum.OPERATION;

    //日志操作类型
    OperationTypeEnum operationType() default OperationTypeEnum.OTHER;

    //日志操作内容
    String operateContext() default "";

    //操作路径
    OperationPathEnum operatePath() default OperationPathEnum.OTHER;

    //是否记录修改前后变动内容
    boolean isSaveUpdateInfo() default false;
}