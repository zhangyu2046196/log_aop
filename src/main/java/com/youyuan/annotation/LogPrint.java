package com.youyuan.annotation;

import java.lang.annotation.*;

/**
 * 类名称：LogPrint <br>
 * 类描述： 日志打印注解 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2023/10/9 15:23<br>
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogPrint {

    /**
     * 日志前缀
     */
    String logPrefix() default "";

    /**
     * 方法名
     */
    String methodName() default "";

    /**
     * 是否展示返回值
     */
    boolean showReturnValue() default true;

}
