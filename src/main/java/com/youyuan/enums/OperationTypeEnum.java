package com.youyuan.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：OperationTypeEnum <br>
 * 类描述： 操作类型枚举 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:30<br>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OperationTypeEnum implements Serializable {

    QUERY("1", "查询"),
    ADD("2", "新增"),
    UPDATE("3", "修改"),
    DELETE("3", "删除"),
    UPLOAD("3", "上传"),
    DOWNLOAD("3", "下载"),
    OTHER("4", "其它");

    private String code;

    private String value;

    OperationTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static OperationTypeEnum getEnum(String code) {
        for (OperationTypeEnum inStorageApplyStatusEnum : values()) {
            if (inStorageApplyStatusEnum.getCode().equals(code)) {
                return inStorageApplyStatusEnum;
            }
        }
        return null;
    }

    public static String getValue(String code) {
        for (OperationTypeEnum value : values()) {
            if (code.equals(value.getCode())) {
                return value.getValue();
            }
        }
        return null;
    }

    /**
     * 方法名: buildEnumName <br>
     * 方法描述: 构建枚举名称集合 <br>
     *
     * @return {@link List<String> 返回结果内容 }
     * @date 创建时间: 2024/3/16 17:02 <br>
     * @author zhangyu
     */
    public static List<String> buildEnumName() {
        return Arrays.stream(values()).map(Enum::toString).collect(Collectors.toList());
    }

}
