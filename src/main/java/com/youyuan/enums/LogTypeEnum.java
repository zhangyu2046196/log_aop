package com.youyuan.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * 类名称：LogTypeEnum <br>
 * 类描述： 日志类型枚举 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:28<br>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LogTypeEnum implements Serializable {

    BUSINESS("1", "业务日志"),
    SECURITY("2", "安全日志"),
    OPERATION("3", "操作日志"),
    OTHER("4", "其它");

    private String code;

    private String value;

    LogTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static LogTypeEnum getEnum(String code) {
        for (LogTypeEnum inStorageApplyStatusEnum : values()) {
            if (inStorageApplyStatusEnum.getCode().equals(code)) {
                return inStorageApplyStatusEnum;
            }
        }
        return null;
    }

    public static String getValue(String code) {
        for (LogTypeEnum value : values()) {
            if (code.equals(value.getCode())) {
                return value.getValue();
            }
        }
        return null;
    }
}