package com.youyuan.enums;

import lombok.Getter;

/**
 * 后台返回结果集枚举
 */
@Getter
public enum ResultEnum {

    /**
     * 失败错误码
     */
    ERROR(500, "操作失败!"),

    /**
     * 公共返回结果（WNF）
     */
    PARAM_ERROR(102, "参数缺失"),
    SUCCESS(200, "操作成功!"),
    DATA_IS_EXCEPTION(500, "数据存在异常!"),
    DATA_NOT_EXIST(201, "数据不存在!");

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
