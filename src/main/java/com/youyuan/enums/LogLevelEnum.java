package com.youyuan.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * 类名称：LogLevelEnum <br>
 * 类描述： 日志级别相关枚举 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:27<br>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LogLevelEnum implements Serializable {

    ALL("1", "ALL"),
    DEBUG("2", "DEBUG"),
    INFO("3", "INFO"),
    WARN("4", "WARN"),
    ERROR("5", "ERROR"),
    FATAL("6", "FATAL"),
    OFF("7", "OFF"),
    OTHER("8", "其他");

    private String code;

    private String value;

    LogLevelEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static LogLevelEnum getEnum(String code) {
        for (LogLevelEnum inStorageApplyStatusEnum : values()) {
            if (inStorageApplyStatusEnum.getCode().equals(code)) {
                return inStorageApplyStatusEnum;
            }
        }
        return null;
    }

    public static String getValue(String code) {
        for (LogLevelEnum value : values()) {
            if (code.equals(value.getCode())) {
                return value.getValue();
            }
        }
        return null;
    }
}