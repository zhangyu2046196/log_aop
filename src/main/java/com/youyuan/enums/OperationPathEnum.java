package com.youyuan.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：OperationPathEnum <br>
 * 类描述： 操作路径枚举 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 17:06<br>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum OperationPathEnum implements Serializable {

    MATERIAL_ADD("1", "食材管理-新增食材"),
    MATERIAL_UPDATE("2", "食材管理-修改食材"),
    MATERIAL_DELETE("3", "食材管理-删除食材"),
    BIND_ADD("5", "绑定关系-新增绑定关系"),
    BIND_UPDATE("6", "绑定关系-修改绑定关系"),
    BIND_DELETE("7", "绑定关系-删除绑定关系"),
    SUPPLIER_ADD("8", "供应商管理-新增供应商"),
    SUPPLIER_UPDATE("9", "供应商管理-修改供应商"),
    SUPPLIER_DELETE("10", "供应商管理-删除供应商"),
    OTHER("11", "其它");

    private String code;

    private String value;

    OperationPathEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static OperationPathEnum getEnum(String code) {
        for (OperationPathEnum inStorageApplyStatusEnum : values()) {
            if (inStorageApplyStatusEnum.getCode().equals(code)) {
                return inStorageApplyStatusEnum;
            }
        }
        return null;
    }

    public static String getValue(String code) {
        for (OperationPathEnum value : values()) {
            if (code.equals(value.getCode())) {
                return value.getValue();
            }
        }
        return null;
    }

    /**
     * 方法名: buildEnumName <br>
     * 方法描述: 构建枚举名称集合 <br>
     *
     * @return {@link List<String> 返回结果内容 }
     * @date 创建时间: 2024/3/16 17:02 <br>
     * @author zhangyu
     */
    public static List<String> buildEnumName() {
        return Arrays.stream(values()).map(OperationPathEnum::getValue).collect(Collectors.toList());
    }

}
