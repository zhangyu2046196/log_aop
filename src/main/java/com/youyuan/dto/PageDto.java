package com.youyuan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 类名称：PageDto <br>
 * 类描述： 分页基础参数 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2023/12/12 15:17<br>
 */
@Data
@ApiModel(value = "分页基础参数", description = "分页基础参数")
public class PageDto implements Serializable {

    private static final long serialVersionUID = 8158642812487376795L;

    /**
     * 页数
     */
    @ApiModelProperty(name = "pageNum", value = "页数", notes = "页数", example = "1", required = true, position = 1)
    private Integer pageNum = 1;

    /**
     * 每页显示记录数
     */
    @ApiModelProperty(name = "pageSize", value = "每页显示记录数", notes = "每页显示记录数", example = "10", required = true,
            position = 2)
    private Integer pageSize = 10;

    /**
     * 偏移量
     */
    @ApiModelProperty(name = "offset", value = "偏移量", notes = "偏移量", hidden = true, position = 3)
    private Integer offset;

    public Integer getOffset() {
        return pageNum != null && pageSize != null ? (pageNum - 1) * pageSize : 0;
    }
}
