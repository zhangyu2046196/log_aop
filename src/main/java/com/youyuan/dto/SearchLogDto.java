package com.youyuan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 类名称：SearchLogDto <br>
 * 类描述： 操作日志搜索Dto对象 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 16:19<br>
 */
@Data
@ApiModel(value = "操作日志搜索Dto对象", description = "操作日志搜索Dto对象")
public class SearchLogDto extends PageDto implements Serializable {

    private static final long serialVersionUID = -4311341123252218309L;
    /**
     * 操作人工号
     */
    @ApiModelProperty(name = "operateCode", value = "操作人工号", notes = "操作人工号", position = 1)
    private String operateCode;

    /**
     * 操作类型
     */
    @ApiModelProperty(name = "operationType", value = "操作类型", notes = "操作类型", position = 2)
    private String operationType;


    /**
     * 操作路径
     */
    @ApiModelProperty(name = "operatePath", value = "操作路径", notes = "操作路径", position = 3)
    private String operatePath;

}
