package com.youyuan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.youyuan.mapper"})
public class LogAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogAopApplication.class, args);
    }

}
