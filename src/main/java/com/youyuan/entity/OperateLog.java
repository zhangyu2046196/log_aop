package com.youyuan.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 类名称：OperateLog <br>
 * 类描述： 操作日志 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 15:43<br>
 */
@Data
public class OperateLog implements Serializable {

    private static final long serialVersionUID = 6005006797553314835L;

    /**
     * 主键
     */
    @ApiModelProperty(name = "operateId", value = "主键")
    @TableId(value = "f_operate_id", type = IdType.AUTO)
    private Long operateId;

    /**
     * logLevel
     */
    @ApiModelProperty(name = "logLevel", value = "日志级别")
    private String logLevel;

    /**
     * 日志类型
     */
    @ApiModelProperty(name = "logType", value = "日志类型")
    private String logType;

    /**
     * 操作类型
     */
    @ApiModelProperty(name = "operationType", value = "操作类型")
    private String operationType;

    /**
     * 操作人名称
     */
    @ApiModelProperty(name = "operateName", value = "操作人名称")
    private String operateName;

    /**
     * 操作人工号
     */
    @ApiModelProperty(name = "operateCode", value = "操作人工号")
    private String operateCode;

    /**
     * 操作路径
     */
    @ApiModelProperty(name = "operatePath", value = "操作路径")
    private String operatePath;

    /**
     * 操作日志内容
     */
    @ApiModelProperty(name = "contexts", value = "操作日志内容")
    private List<String> contexts;

}