package com.youyuan.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 类名称：OperateDetailLog <br>
 * 类描述： 操作日志详细内容 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/15 16:14<br>
 */
@Data
public class OperateDetailLog implements Serializable {

    private static final long serialVersionUID = 8084269568395509516L;

    /**
     * 主键
     */
    @ApiModelProperty(name = "operateDetailId", value = "主键")
    private Long operateDetailId;

    /**
     * 操作日志主表主键
     */
    @ApiModelProperty(name = "operateId", value = "操作日志主表主键")
    private Long operateId;

    /**
     * 操作内容
     */
    @ApiModelProperty(name = "operateContext", value = "操作内容")
    private String operateContext;

}