package com.youyuan.entity;

import com.youyuan.annotation.IsSaveUpdateLog;
import lombok.Data;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.Serializable;

/**
 * 类名称：LogTest <br>
 * 类描述： 表单实体测试修改前后日志对比 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/18 10:04<br>
 */
@Data
public class LogTest implements Serializable {

    private static final long serialVersionUID = 351681802599006243L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 姓名
     */
    @IsSaveUpdateLog(name = "姓名")
    private String name;

    /**
     * 年龄
     */
    @IsSaveUpdateLog(name = "年龄")
    private Integer age;

    /**
     * 地址
     */
    private String address;

}
