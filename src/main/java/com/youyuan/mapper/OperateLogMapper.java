package com.youyuan.mapper;

import com.youyuan.dto.SearchLogDto;
import com.youyuan.entity.OperateLog;
import com.youyuan.vo.OperateLogVo;

import java.util.List;

/**
 * 类名称：OperateLogMapper <br>
 * 类描述： 日志主表mapper 接口 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 11:39<br>
 */
public interface OperateLogMapper {

    /**
     * 方法名: batchInsert <br>
     * 方法描述: 批量保存操作日志主表 <br>
     *
     * @param list 请求参数信息
     * @return {@link int > 0 成功 int <= 0 失败 }
     * @date 创建时间: 2024/3/16 13:05 <br>
     * @author zhangyu
     */
    int batchInsert(List<OperateLog> list);

    /**
     * 方法名: findList <br>
     * 方法描述: 查询操作日志列表 <br>
     *
     * @param searchLogDto 请求参数信息
     * @return {@link List<OperateLogVo> 返回结果集合 }
     * @date 创建时间: 2024/3/16 16:28 <br>
     * @author zhangyu
     */
    List<OperateLogVo> findList(SearchLogDto searchLogDto);

}
