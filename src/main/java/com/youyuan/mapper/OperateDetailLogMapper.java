package com.youyuan.mapper;

import com.youyuan.entity.OperateDetailLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 类名称：OperateDetailLogMapper <br>
 * 类描述： 操作日志详细日志mapper 接口 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 11:41<br>
 */
public interface OperateDetailLogMapper {

    /**
     * 方法名: batchInsert <br>
     * 方法描述: 批量保存操作日志详单数据 <br>
     *
     * @param operateDetailLogs 请求参数信息
     * @return {@link int > 0 成功 int <= 0 失败 }
     * @date 创建时间: 2024/3/16 13:11 <br>
     * @author zhangyu
     */
    int batchInsert(@Param("operateDetailLogs") List<OperateDetailLog> operateDetailLogs);

    /**
     * 方法名: findListById <br>
     * 方法描述: 根据操作主键查询列表 <br>
     *
     * @param operateId 操作日志主键
     * @return {@link List<String> 返回结果集合 }
     * @date 创建时间: 2024/3/16 16:38 <br>
     * @author zhangyu
     */
    List<String> findListById(Long operateId);

}
