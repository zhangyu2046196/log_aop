package com.youyuan.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.youyuan.constants.LogConstant;
import com.youyuan.dto.SearchLogDto;
import com.youyuan.entity.OperateDetailLog;
import com.youyuan.entity.OperateLog;
import com.youyuan.enums.OperationPathEnum;
import com.youyuan.enums.OperationTypeEnum;
import com.youyuan.enums.ResultEnum;
import com.youyuan.mapper.OperateDetailLogMapper;
import com.youyuan.mapper.OperateLogMapper;
import com.youyuan.service.OperateLogService;
import com.youyuan.vo.OperateLogVo;
import com.youyuan.vo.ResultVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：OperateLogServiceImpl <br>
 * 类描述： 操作日志业务接口实现类 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 13:17<br>
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class OperateLogServiceImpl implements OperateLogService {

    private final OperateLogMapper operateLogMapper;
    private final OperateDetailLogMapper operateDetailLogMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveLog(List<OperateLog> operateLogs) {
        int result = operateLogMapper.batchInsert(operateLogs);
        if (result > LogConstant.ZERO) {
            List<OperateDetailLog> detailLogs = CollUtil.newArrayList();
            for (OperateLog operateLog : operateLogs) {
                List<OperateDetailLog> collect = operateLog.getContexts().stream().map(str -> {
                    OperateDetailLog operateDetailLog = new OperateDetailLog();
                    operateDetailLog.setOperateId(operateLog.getOperateId());
                    operateDetailLog.setOperateContext(str);
                    return operateDetailLog;
                }).collect(Collectors.toList());
                detailLogs.addAll(collect);
            }
            operateDetailLogMapper.batchInsert(detailLogs);
        }
    }

    @Override
    public ResultVo findList(SearchLogDto searchLogDto) {
        PageHelper.startPage(searchLogDto.getPageNum(), searchLogDto.getPageSize());
        List<OperateLogVo> operateLogVos = operateLogMapper.findList(searchLogDto);
        PageInfo<OperateLogVo> page = new PageInfo(operateLogVos);
        page.setList(operateLogVos);
        return ResultVo.builder().code(ResultEnum.SUCCESS.getCode()).msg(ResultEnum.SUCCESS.getMessage()).data(page).build();
    }

    @Override
    public ResultVo findOperateTypeList() {
        return ResultVo.builder().code(ResultEnum.SUCCESS.getCode()).msg(ResultEnum.SUCCESS.getMessage()).data(OperationTypeEnum.buildEnumName()).build();
    }

    @Override
    public ResultVo findOperatePathList() {
        return ResultVo.builder().code(ResultEnum.SUCCESS.getCode()).msg(ResultEnum.SUCCESS.getMessage()).data(OperationPathEnum.buildEnumName()).build();
    }
}
