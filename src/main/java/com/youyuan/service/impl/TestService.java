package com.youyuan.service.impl;

import com.youyuan.annotation.Log;
import com.youyuan.constants.LogConstant;
import com.youyuan.entity.LogTest;
import com.youyuan.enums.OperationPathEnum;
import com.youyuan.enums.OperationTypeEnum;
import com.youyuan.util.ContextHandler;
import org.springframework.stereotype.Service;

/**
 * 类名称：TestService <br>
 * 类描述： 测试记录日志 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/18 9:29<br>
 */
@Service
public class TestService {

    /**
     * 方法名: add <br>
     * 方法描述: 新增数据打印日志 <br>
     *
     * @date 创建时间: 2024/3/18 9:59 <br>
     * @author zhangyu
     */
    @Log(operationType = OperationTypeEnum.ADD, operateContext = "新增", operatePath = OperationPathEnum.SUPPLIER_ADD,
            isSaveUpdateInfo = true)
    public void add() {

        System.out.println("123");
        LogTest logTest = new LogTest();
        logTest.setName("王五");
        logTest.setAge(28);
        logTest.setAddress("河北省保定市");

        //记录操作日志
        ContextHandler.set(LogConstant.LOG_NEW_DATA, logTest);

    }

    /**
     * 方法名: update <br>
     * 方法描述: 修改数据记录修改前后日志 <br>
     *
     * @date 创建时间: 2024/3/18 10:41 <br>
     * @author zhangyu
     */
    @Log(operationType = OperationTypeEnum.UPDATE, operateContext = "修改", operatePath =
            OperationPathEnum.SUPPLIER_UPDATE, isSaveUpdateInfo = true)
    public void update() {

        LogTest oldLogTest = new LogTest();
        oldLogTest.setId(2);
        oldLogTest.setName("王五");
        oldLogTest.setAge(28);
        oldLogTest.setAddress("河北省保定市");

        LogTest newLogTest = new LogTest();
        newLogTest.setId(2);
        newLogTest.setName("小明");
        newLogTest.setAge(30);
        newLogTest.setAddress("河北省保定市");


        ContextHandler.set(LogConstant.LOG_OLD_DATA, oldLogTest);
        ContextHandler.set(LogConstant.LOG_NEW_DATA, newLogTest);

    }

    /**
     * 方法名: del <br>
     * 方法描述: 删除数据记录日志 <br>
     *
     * @date 创建时间: 2024/3/18 10:45 <br>
     * @author zhangyu
     */
    @Log(operationType = OperationTypeEnum.DELETE, operateContext = "删除", operatePath =
            OperationPathEnum.SUPPLIER_DELETE, isSaveUpdateInfo = true)
    public void del() {

        LogTest logTest = new LogTest();
        logTest.setName("王五");
        logTest.setAge(28);
        logTest.setAddress("河北省保定市");
        ContextHandler.set(LogConstant.LOG_NEW_DATA, logTest);

    }

}
