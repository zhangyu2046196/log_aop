package com.youyuan.service;

import com.youyuan.dto.SearchLogDto;
import com.youyuan.entity.OperateLog;
import com.youyuan.vo.ResultVo;

import java.util.List;

/**
 * 类名称：OperateLogService <br>
 * 类描述： 操作日志业务接口 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 13:15<br>
 */
public interface OperateLogService {

    /**
     * 方法名: saveLog <br>
     * 方法描述: 保存操作日志 <br>
     *
     * @param operateLogs 请求参数信息
     * @date 创建时间: 2024/3/16 13:16 <br>
     * @author zhangyu
     */
    void saveLog(List<OperateLog> operateLogs);

    /**
     * 方法名: findList <br>
     * 方法描述: 查询操作日志列表 <br>
     *
     * @param searchLogDto 请求参数信息
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/16 16:26 <br>
     * @author zhangyu
     */
    ResultVo findList(SearchLogDto searchLogDto);

    /**
     * 方法名: findOperateTypeList <br>
     * 方法描述: 查询日志操作类型 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/16 16:58 <br>
     * @author zhangyu
     */
    ResultVo findOperateTypeList();

    /**
     * 方法名: findOperatePathList <br>
     * 方法描述: 查询操作日志路径 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/16 17:22 <br>
     * @author zhangyu
     */
    ResultVo findOperatePathList();

}
