package com.youyuan.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 类名称：OperateLogVo <br>
 * 类描述： 操作日志视图对象 <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 16:10<br>
 */
@Data
@ApiModel(value = "操作日志视图对象", description = "操作日志视图对象")
public class OperateLogVo implements Serializable {

    private static final long serialVersionUID = -7883948236940427267L;

    /**
     * logLevel
     */
    @ApiModelProperty(name = "memberName", value = "日志级别", notes = "日志级别", position = 1)
    private String logLevel;

    /**
     * 日志类型
     */
    @ApiModelProperty(name = "logType", value = "日志类型", notes = "日志类型", position = 2)
    private String logType;

    /**
     * 操作类型
     */
    @ApiModelProperty(name = "operationType", value = "操作类型", notes = "操作类型", position = 3)
    private String operationType;

    /**
     * 操作人名称
     */
    @ApiModelProperty(name = "operateName", value = "操作人名称", notes = "操作人名称", position = 5)
    private String operateName;

    /**
     * 操作人工号
     */
    @ApiModelProperty(name = "operateCode", value = "操作人工号", notes = "操作人工号", position = 6)
    private String operateCode;

    /**
     * 操作路径
     */
    @ApiModelProperty(name = "operatePath", value = "操作路径", notes = "操作路径", position = 7)
    private String operatePath;

    /**
     * 操作日志内容
     */
    @ApiModelProperty(name = "contexts", value = "操作日志内容", notes = "操作日志内容", position = 8)
    private List<String> contexts;

    /**
     * 操作时间
     */
    @ApiModelProperty(name = "operateTime", value = "操作时间", notes = "操作时间", position = 9)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String operateTime;

    /**
     * 主键
     */
    @ApiModelProperty(name = "operateId", value = "主键", notes = "主键", position = 10)
    private Long operateId;

}