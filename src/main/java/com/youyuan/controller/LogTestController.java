package com.youyuan.controller;

import com.youyuan.annotation.LogPrint;
import com.youyuan.constants.LogConstant;
import com.youyuan.service.impl.TestService;
import com.youyuan.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类名称：LogTestController <br>
 * 类描述： 日志测试API <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/18 11:32<br>
 */
@RestController
@RequestMapping("/log")
@Api(tags = "日志测试API", value = "日志测试API")
@RequiredArgsConstructor
@Slf4j
public class LogTestController {

    private final TestService testService;

    /**
     * 方法名: add <br>
     * 方法描述: 添加数据 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/18 11:34 <br>
     * @author zhangyu
     */
    @LogPrint(logPrefix = LogConstant.LOG_PREFIX, methodName = "添加数据")
    @GetMapping("/add")
    @ApiOperation(value = "添加数据", notes = "添加数据", httpMethod = "GET", response = Void.class)
    public void add() {
        testService.add();
    }

    /**
     * 方法名: update <br>
     * 方法描述: 修改数据 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/18 11:34 <br>
     * @author zhangyu
     */
    @LogPrint(logPrefix = LogConstant.LOG_PREFIX, methodName = "修改数据")
    @GetMapping("/update")
    @ApiOperation(value = "修改数据", notes = "修改数据", httpMethod = "GET", response = Void.class)
    public void update() {
        testService.update();
    }

    /**
     * 方法名: del <br>
     * 方法描述: 删除数据 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/18 11:34 <br>
     * @author zhangyu
     */
    @LogPrint(logPrefix = LogConstant.LOG_PREFIX, methodName = "删除数据")
    @GetMapping("/del")
    @ApiOperation(value = "删除数据", notes = "删除数据", httpMethod = "GET", response = Void.class)
    public void del() {
        testService.del();
    }

}
