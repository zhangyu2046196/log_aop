package com.youyuan.controller;

import com.youyuan.annotation.LogPrint;
import com.youyuan.constants.LogConstant;
import com.youyuan.dto.SearchLogDto;
import com.youyuan.service.OperateLogService;
import com.youyuan.vo.OperateLogVo;
import com.youyuan.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 类名称：OperateLogController <br>
 * 类描述： 操作日志API <br>
 *
 * @author zhangyu
 * @version 1.0.0
 * @date 创建时间：2024/3/16 16:15<br>
 */
@RestController
@RequestMapping("/operate/log")
@Api(tags = "操作日志API", value = "操作日志API")
@RequiredArgsConstructor
@Slf4j
public class OperateLogController {

    private final OperateLogService operateLogService;

    @LogPrint(logPrefix = LogConstant.LOG_PREFIX, methodName = "查询操作日志列表")
    @PostMapping("/findList")
    @ApiOperation(value = "查询操作日志列表", notes = "查询操作日志列表", httpMethod = "POST", response = OperateLogVo.class)
    public ResultVo findList(@ApiParam(value = "查询参数", required = true)
                             @RequestBody SearchLogDto searchLogDto) {
        return operateLogService.findList(searchLogDto);
    }

    /**
     * 方法名: findOperateTypeList <br>
     * 方法描述: 查询日志操作类型 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/16 16:57 <br>
     * @author zhangyu
     */
    @LogPrint(logPrefix = LogConstant.LOG_PREFIX, methodName = "查询日志操作类型")
    @GetMapping("/operateTypeList")
    @ApiOperation(value = "查询日志操作类型", notes = "查询日志操作类型", httpMethod = "GET", response = String.class)
    public ResultVo findOperateTypeList() {
        return operateLogService.findOperateTypeList();
    }

    /**
     * 方法名: operatePathList <br>
     * 方法描述: 查询日志操作路径 <br>
     *
     * @return {@link ResultVo 返回结果内容 }
     * @date 创建时间: 2024/3/16 17:21 <br>
     * @author zhangyu
     */
    @LogPrint(logPrefix = LogConstant.LOG_PREFIX, methodName = "查询日志操作路径")
    @GetMapping("/operatePathList")
    @ApiOperation(value = "查询日志操作路径", notes = "查询日志操作路径", httpMethod = "GET", response = String.class)
    public ResultVo findOperatePathList() {
        return operateLogService.findOperatePathList();
    }

}
